## TODO
* add the name over the corresponding player hand
* Move dynamite to center of screen
* Make dynamite larger
* Make throw animation work for both players
* There are disconnect issues

### DONE 2019-03-02 
* Make buttons squares and a little taller, so a tall rectangle
* Remove blue flash on mobile when pressing button
* Reduce the lime green height 
* Disable button access by shading after the selection is made to clearly indicate not being accessible until the new game starts
* Add the texture that allows for the button to depress when selected
* Create all three throw animations