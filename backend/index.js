const io = require(`socket.io`)();
const port = 40420;

let rooms = [];

const createRoom = (roomId, player) => rooms.push({
    id: roomId,
    players: [ player ],
    attacks: [],
    countingDown: false
});

const getId = () => Math
    .random()
    .toString(36)
    .substring(2, 25)
    .split(``)
    .map(c => Math.random() >= .5 ? c.toLocaleUpperCase() : c.toLocaleLowerCase())
    .join(``);

const removeDisconnected = input => {
    if (!input) return;

    const isDisconnected = i => i.client.disconnected;
    while (input.some(isDisconnected)) {
        const index = input.findIndex(isDisconnected);
        console.log(`purging client id ${input[index].client.id}`);
        input.splice(index, 1);
    }
};

const getResults = (player1, player2) => {
    const rules = {
        rock: { beats: `scissors`, rule: `Rock smashes scissors` },
        paper: { beats: `rock`, rule: `Paper covers rock` },
        scissors: { beats: `paper`, rule: `Scissors cuts paper` }
    };

    if (!player2) {
        return `${player1.name} wins by forfeit!`;
    }

    if (player1.attack === player2.attack) {
        return `Draw, ${player1.attack} and ${player2.attack}`;
    } else if (player2.attack === rules[player1.attack].beats) {
        return `${player1.name} wins! ${rules[player1.attack].rule}!`;
    }
    return `${player2.name} wins! ${rules[player2.attack].rule}!`;
};

io.on(`connection`, client => {

    const startCountdown = room => {
        console.log(`client is subscribing to timer`);
        const existingRoom = rooms.find(r => r.id === room);
        existingRoom.countingDown = true;
        const interval = 250;
        let timeRemaining = 3.1 * 1000;
        sendCast({ type: `msg`, payload: `starting countdown` });
        const countdown = setInterval(() => {
            timeRemaining -= interval;
            sendCast({ type: `timer`, payload: `${(timeRemaining / 1000).toFixed(2)} seconds left!` });
            if (timeRemaining <= 0) {
                const [ player1, player2 ] = existingRoom.attacks;
                const payload = getResults(player1, player2);
                sendCast({ type: `result`, payload });
                clearInterval(countdown);
                existingRoom.countingDown = false;
                existingRoom.attacks = [];
            }
        }, interval);
    };

    const sendCast = cast => {
        console.log(`sending message: ${cast}`);
        const { id } = client;
        const room = rooms.find(r => r.players.some(p => p.client.id === id));
        if (room) {
            room.players
                .forEach(p => p.client.emit(`newCast`, cast));
        }
    };

    console.log(`a user connected`);
    client.on(`subscribeToTimer`, () => {
                
    });

    client.on(`getPlayerId`, () => {
        console.log(`creating client identity`);
        const playerId = getId();
        client.emit(`newPlayerId`, playerId);
    });

    client.on(`joinRoom`, (roomId, playerId) => {
        console.log(roomId ? `joinging room` : `creating room `);
       
        const player = { id: playerId, client };
        const existingRoom = rooms.find(r => r.id === roomId) || { };
        removeDisconnected(existingRoom.players);

        const roomOpen = existingRoom.players && existingRoom.players.length < 2;

        if (existingRoom && roomOpen) {
            existingRoom.players.push(player);
        } else {
            roomId = roomId && roomOpen ? roomId : getId();
            createRoom(roomId, player);
        }
        
        client.emit(`newRoom`, roomId);
    });

    client.on(`sendAttack`, ({ attack, name, room, playerId }) => {
        const existingRoom = rooms.find(r => r.id === room);
        if (!existingRoom.countingDown) {
            startCountdown(room);
        }
        if (existingRoom && !existingRoom.attacks.some(a => a.playerId === playerId)) {
            existingRoom.attacks.push({ name, attack, playerId });
        }
    });
});

io.listen(port, () => {
    console.log(`listening on  ${port}`);
});