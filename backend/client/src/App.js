import React, { Component } from "react";
import { joinRoom, getPlayerId, sendCast } from "./api";
import Cookies from "universal-cookie";
import "./App.css";
import rps from "./animation.gif";
import sample from "./sample.png";
import timer from "./rpssemifinal.gif";
const roomRX = /room\/([\w]+)/;
class App extends Component {
    state = {
        room: "",
        name: "",
        messages: "",
        playerId: "",
        loggedIn: false,
        timer: "",
        timerPath: "",
        attack: ""
    };
 
    componentDidMount = () => {
        const cookies = new Cookies();
        const name = cookies.get("name") || "";
        const playerId = cookies.get("playerId") || "";

        this.setIdentity({ name, playerId });
        this.sussRoomId();      
    }

    setIdentity = ({ name, playerId }) => {
        if (!playerId) {
            getPlayerId((err, playerId) => {
                this.setState({ playerId, name }, this.persistPlayerState);
            });
        } else {
            this.setState({ playerId, name }, this.persistPlayerState);
        }
    }

    persistPlayerState = () => {
        const cookies = new Cookies();
        const { name, playerId } = this.state;
        cookies.set("name", name || "");
        cookies.set("playerId", playerId || "");
    }

    login = () => this.setState({ loggedIn: true }, this.persistPlayerState)

    sussRoomId = () => {
        
        const path = window.location.pathname;

        const [ , id ] = roomRX.exec(path) || [];
      
        joinRoom(this.setRoom, this.handleCast, id);

    }

    handleCast = (err, cast) => {
        const { timerPath } = this.state;
        switch (cast.type) {
            case "msg":
                this.setState({ messages: cast.payload });
                break;
            case "result":
                this.setState({ messages: cast.payload, timer: "", attack: "" });
                break;
            case "timer":
                this.setState({ timer: cast.payload, timerPath: this.state.timer ? timerPath : timer + `?a=${Math.random()}` });
                break;
        
            default:
                break;
        }
    }

    setRoom = (err, room) => this.setState({ room }, () => {
              
        const path = window.location.pathname;

        const [ match ] = roomRX.exec(path) || [];

        const newPath = match ? this.state.room : `room/${this.state.room}`;

        window.history.replaceState(null, "", newPath);
    })

    handleChange = ({ target }) => {
        const { name, value } = target;
        this.setState({ [name]: value });
    }
 

    render() {
        return (
            <div className="App">
                {
                    this.state.timer ?
                        <div className="Alert">
                            <img src={this.state.timerPath} />
                        </div>  
                        :
                        null        
                }
                <div className="Grid">

                    <div className="Header Container">
                        {
                            this.state.loggedIn ?
                                <h4 className="Name">{this.state.name}</h4>:
                                <fieldset>
                                    <label>Login:</label>
                                    <input
                                        name="name"
                                        onChange={this.handleChange}
                                        value={this.state.name || ""}
                                    />
                                    <button className="Login-Button" onClick={this.login}>Go</button>
                                </fieldset>
                        }
                        
                    </div>

                    <div className="Opponent Container">
                        <textarea
                            disabled
                            value={this.state.messages}
                        />
                    </div>

                    <div className="Player Container">
                        <div className="Medium">
                            <img src={sample} />
                        </div>
                                        
                       
                     
                        
                    </div>

                    <div className="Controls Container">
                        {
                            [ "rock", "paper", "scissors" ].map(attack => {
                                const { name, room, playerId } = this.state;
                                return (
                                    <div className={`Control ${this.state.attack ? "Disabled" : ""} ${this.state.attack === attack ? "Selected" : ""}`} onClick={() => {
                                        if (!this.state.attack) this.setState({ attack });
                                        sendCast(null, { attack, name, room, playerId });
                                    }}>
                                        <div className="Control-Contents">
                                            <span className="Control-Text">{attack}</span>
                                        </div>
                                        
                                    </div>
                                );
                            })
                        }
                    </div>

                </div>

            </div>
        );
    }
}

export default App;
