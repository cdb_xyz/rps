import openSocket from "socket.io-client";
import config from "./apiconfig.json";
const socket = openSocket(`${config.host}:40420`);

const subscribeToTimer = cb => {
    socket.on("timer", timestamp => cb(null, timestamp));
    socket.emit("subscribeToTimer");
};

const joinRoom = (roomCB,castCB, room) => {
    socket.on("newRoom", room => roomCB(null, room));
    socket.on("newCast", cast => castCB(null, cast));
    socket.emit("joinRoom", room);
};

const getPlayerId = cb => {
    socket.on("newPlayerId", id => cb(null, id));
    socket.emit("getPlayerId");
};

const sendAttack = (cb, attack) => {
    socket.emit("sendAttack", attack);
};

export {
    subscribeToTimer,
    joinRoom,
    getPlayerId,
    sendAttack as sendCast,
};